﻿using Nancy;
using Nancy.TinyIoc;
using Notify.Authentication;

namespace Notify
{
    public class Bootstrapper : DefaultNancyBootstrapper
    {
        protected override void ApplicationStartup(TinyIoCContainer container, Nancy.Bootstrapper.IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);

            pipelines.EnableWindowsAuthentication(new WindowsAuthenticationConfiguration(container.Resolve<IWindowsUserValidator>()));
        }
    }
}