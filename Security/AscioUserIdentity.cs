﻿using System.Collections.Generic;
using Notify.Security;

namespace Notify.Security
{
    public class AscioUserIdentity : IWindowsUserIdentity
    {
        public string UserName { get; set; }

        public IEnumerable<string> Claims { get; set; }

        public string EmailAddress { get; set; }

        public string GivenName { get; set; }

        public string DisplayName { get; set; }
    }
}