﻿using Notify.Authentication;
using Notify.Security;
using System;
using System.DirectoryServices.AccountManagement;
using NLog;

namespace Notify.Security
{
    public class UserValidator : IWindowsUserValidator
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public IWindowsUserIdentity Validate(string username)
        {
            PrincipalContext context = new PrincipalContext(ContextType.Domain, "10.61.128.98");
            try
            {
                logger.Debug("Username is {0}", username);
                var ldapUser = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, username);
                return new AscioUserIdentity
                {
                    UserName = username,
                    GivenName = ldapUser.GivenName,
                    DisplayName = ldapUser.DisplayName,
                    EmailAddress = ldapUser.EmailAddress
                };
            } catch(Exception e)
            {
                logger.Fatal(e);
                return null;
            }            
         
        }
    }
}