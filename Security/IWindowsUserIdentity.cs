﻿using Nancy.Security;

namespace Notify.Security
{
    public interface IWindowsUserIdentity : IUserIdentity
    {
        string EmailAddress { get; set; }

        string GivenName { get; set; }

        string DisplayName { get; set; }
    }
}
