﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy;
using Nancy.Security;
using Notify.Security;

namespace Notify
{
    public class MainModule : NancyModule
    {
        public MainModule()
        {
            this.RequiresAuthentication();            

            Get["/"] = _ =>
            {
                var currentUser = (IWindowsUserIdentity)Context.CurrentUser;
                return View["index", currentUser];
            };

            Get["/profile"] = _ =>
            {
                var currentUser = (IWindowsUserIdentity)Context.CurrentUser;
                return View["profile", currentUser];
            };
        }
    }
}