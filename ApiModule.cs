﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy;

namespace Notify
{
    public class ApiModule : NancyModule
    {
        public ApiModule() : base("/api")
        {

        }
    }
}