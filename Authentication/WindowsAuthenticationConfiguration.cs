﻿using System;
using Nancy.Authentication.Basic;

namespace Notify.Authentication
{
    public class WindowsAuthenticationConfiguration
    {
        public IWindowsUserValidator UserValidator { get; private set; }

        public WindowsAuthenticationConfiguration(IWindowsUserValidator userValidator)
        {
            if (userValidator == null)
                throw new ArgumentNullException("userValidator");

            this.UserValidator = userValidator;
        }
    }
}