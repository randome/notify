﻿using System;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Extensions;
using Nancy.Security;
using System.Security.Principal;
using NLog;

namespace Notify.Authentication
{
    public static class WindowsAuthentication
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static void Enable(IPipelines pipelines, WindowsAuthenticationConfiguration configuration)
        {
            if(pipelines == null)
            {
                throw new ArgumentNullException("pipelines");
            }

            if(configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }
            
            pipelines.BeforeRequest.AddItemToStartOfPipeline(GetCredentials(configuration));
        }

        public static void Enable(INancyModule module, WindowsAuthenticationConfiguration configuration)
        {
            if (module == null)
            {
                throw new ArgumentNullException("module");
            }

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            module.RequiresAuthentication();
            module.Before.AddItemToStartOfPipeline(GetCredentials(configuration));
        }

        private static Func<NancyContext, Response> GetCredentials(WindowsAuthenticationConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            return context =>
                {
                    IdentifyUser(context, configuration);
                    return null;
                };
        }

        private static void IdentifyUser(NancyContext context, WindowsAuthenticationConfiguration configuration)
        {
            var identity = GetIdentity();
            logger.Debug("IdentityName '{0}', isAuth:{1}", identity.Identity.Name, identity.Identity.IsAuthenticated);
            if (identity != null && identity.Identity.IsAuthenticated)
            {
                var user = configuration.UserValidator.Validate(identity.Identity.Name);
                context.CurrentUser = user;
            }
        }

        private static IPrincipal GetIdentity()
        {
            if (System.Web.HttpContext.Current != null)
            {
                return System.Web.HttpContext.Current.User;
            }

            return new WindowsPrincipal(WindowsIdentity.GetCurrent());
        }
    }
}