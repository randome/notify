﻿using Nancy;
using Nancy.Bootstrapper;

namespace Notify.Authentication
{
    public static class WindowsHttpExtensions
    {
        public static void EnableWindowsAuthentication(this INancyModule module, WindowsAuthenticationConfiguration configuration)
        {
            WindowsAuthentication.Enable(module, configuration);
        }

        public static void EnableWindowsAuthentication(this IPipelines pipeline, WindowsAuthenticationConfiguration configuration)
        {
            WindowsAuthentication.Enable(pipeline, configuration);
        }
    }
}