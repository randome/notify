﻿using Notify.Security;

namespace Notify.Authentication
{
    public interface IWindowsUserValidator
    {
        IWindowsUserIdentity Validate(string username);
    }
}
